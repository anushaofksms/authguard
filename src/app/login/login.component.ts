import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService } from './../dashboard.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  isFormSubmit:boolean = false;
  loading:any;
  loginData:any;
  count=0;
  arr=["anusha","rakesh","mukku"];
  constructor(private router:Router,
              private dashboardService:DashboardService,
              private toastr: ToastrService) { }

  ngOnInit() {
  }
  show()
  {
    console.log(this.arr[this.count]);
    this.count++;
  }
  
  login(data)
  {
      // console.log(data);
      // "email": "eve.holt@reqres.in",
      // "password": "cityslicka"
    this.isFormSubmit = true;
    if(data.invalid === false)
    {
      this.loading = true;
      
      let loggedData = {
        "email":data.value.email,
        "password":"cityslicka"
      }
      this.dashboardService.getLoggin(loggedData).subscribe(response =>{
        this.loginData = response
        this.toastr.success('Success!', response['token']);
        this.loading = false;
        this.router.navigate(['/dashboard/list']);
      }, error => {
        this.toastr.error(error.error.error,'Error!');
        this.loading = false;
        console.log('==Get error Details==', error)
      })
    }

  }
}
