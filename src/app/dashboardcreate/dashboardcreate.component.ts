import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dashboardcreate',
  templateUrl: './dashboardcreate.component.html',
  styleUrls: ['./dashboardcreate.component.css']
})
export class DashboardcreateComponent implements OnInit {
  
  @Input() uerDetail:any;
  @Output() editedDetals = new EventEmitter<any>();
  // @Output() buttonClicked = new EventEmitter<boolean>();
  buttonClicked:boolean;

  fetchData={
    userId:"",
    title:"",
    body:""
  }

  constructor() { }

  ngOnInit() {
    this.buttonClicked=true;
  }

  ngOnChanges(changes) {
    this.uerDetail = changes.uerDetail.currentValue;
  }
  
  editData()
  {
     this.fetchData.userId = this.uerDetail.userId;
     this.fetchData.title = this.uerDetail.title;
     this.fetchData.body = this.uerDetail.body;
  }

  editedData(data)
  {
    this.editedDetals.emit(data);
     this.buttonClicked=false;
    // console.log(this.editedDetals);
  }
}
