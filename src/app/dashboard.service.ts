import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private httpClient:HttpClient) { }

  getLoggin(data)
  {
    return this.httpClient.post('https://reqres.in/api/login', data);
  }

  getUserData()
  {
    return this.httpClient.get('https://jsonplaceholder.typicode.com/posts');
  }
  

  getUserDataById(id)
  {
    return this.httpClient.get('https://jsonplaceholder.typicode.com/posts/'+id);
  }

  postUserData(addData)
  {
    return this.httpClient.post('https://jsonplaceholder.typicode.com/posts', addData)
  }

  editUserData(data,userId)
  {
    return this.httpClient.put('https://jsonplaceholder.typicode.com/posts/'+userId, data);
  }
}
