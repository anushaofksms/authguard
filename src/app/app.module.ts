import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardlistComponent } from './dashboardlist/dashboardlist.component';
import { DashboardcreateComponent } from './dashboardcreate/dashboardcreate.component';
import { ModelComponent } from './model/model.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard/list', component: DashboardlistComponent },
  { path: 'dashboard/create', component: DashboardcreateComponent }
  
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardlistComponent,
    DashboardcreateComponent,
    ModelComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({positionClass: 'toast-top-center'}),
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
