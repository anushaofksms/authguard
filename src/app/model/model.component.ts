import { Component, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.css']
})
export class ModelComponent implements OnInit {
  
  jobs= [
    { id: 1, name: 'Help Desk' },
    { id: 2, name: 'HR' },
    { id: 3, name: 'IT' },
    { id: 4, name: 'Payroll' }
  ];

  @Output() AddUserData = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  addData(data)
  {
    //  console.log(data);
    this.AddUserData.emit(data);
  }
}
