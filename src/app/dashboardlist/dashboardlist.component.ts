import { Component, OnInit } from '@angular/core';
import { DashboardService } from './../dashboard.service';

@Component({
  selector: 'app-dashboardlist',
  templateUrl: './dashboardlist.component.html',
  styleUrls: ['./dashboardlist.component.css']
})
export class DashboardlistComponent implements OnInit {
  
  listUser:any;
  searchTerm:string;
  usr:string;
  message:boolean;
  arr=[1,2,3,4,5,6,7,8]
  temp:any
  uerDetail:any;
  showUserDetail:boolean;
  showAdd:boolean;
  listData:boolean;
  postData:any;
  clickedButton:boolean;
  constructor(private dashboardService:DashboardService) { }

  ngOnInit() {
    this.getUserData()
    this.showUserDetail = false;
    this.showAdd = false;
    this.listData = true;
    }

  getUserData()
  {
    this.dashboardService.getUserData().subscribe(response =>{
      this.listUser = response
        // console.log(this.listUser)
      this.temp=this.listUser
      
    }, error => {
      console.log('==Get error Details==', error)
    })
  }
  
  // inputData()
  // {
  //   // console.log(this.usr);
  //   // this.arr.filter(item =>{
  //   //   if(item < 3)
  //   //    console.log(item);
  //   // })

  //    this.listUser = this.listUser.filter(item =>{
  //     // if(item.name == this.usr)
  //     // {
  //       return this.listUser.filter(employee =>
  //         employee.name.toLowerCase().indexOf(this.usr.toLowerCase()) !== -1);
  //       // this.listUser =  []
  //       // this.listUser.push(item)
  //     // } 
  //   })
  // }

  inputData(event) {
    // console.log(event);
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function (d) {
      // console.log(d)
      return (d.year.toString().toLowerCase().indexOf(val) !== -1 || !val || d.name.toLowerCase().indexOf(val) !== -1 || !val);
    });
    this.listUser = temp;
  }

  // checkArr(item)
  // {
  //    if(item<5)
  //    {
  //      return item;
  //    }
  // }

  getUserDetail(data)
  {
    // this.uerDetail.id = data.id;
    // this.uerDetail.name = data.name;
    // this.uerDetail.name = data.name;
    // console.log(data);
    this.dashboardService.getUserDataById(data.id).subscribe(response=>{
    this.uerDetail = response;
    // console.log(this.uerDetail);
    this.showUserDetail = true;
    })
    
  }

  addUser()
  {
    this.showAdd = true;
    this.listData = false;
  }
  
  getAddedData(recieved)
  {
    //  console.log(recieved);
     let data = {
       "userId":recieved.userId,
       "title":recieved.title,
       "body":recieved.body
     }
    //  console.log(data);
     this.dashboardService.postUserData(data).subscribe(response =>{
      this.postData = response
       console.log('fiuhgjfh',this.postData)
      //  this.listData.push(this.postData);
      this.getUserData();
      this.listData = true;
       this.showAdd = false;
    }, error => {
      console.log('==Get error Details==', error)
    })
  }
  
  getClickedValue(clicked)
  {
    //  console.log(clicked)
    this.clickedButton = clicked;
  }

  getDataFromChild(edited)
  {
    let edit={
      userId:edited.userId,
      title:edited.title,
      body:edited.body
    }
    console.log(edit);
    this.dashboardService.editUserData(edit,edited.userId).subscribe(response =>{
      // this.clickedButton = false;
       this.listData = true;
       this.showAdd = false;
    }, error => {
      console.log('==Get error Details==', error)
    })
  }

 
}
